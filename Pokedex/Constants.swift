//
//  Constants.swift
//  Pokedex
//
//  Created by Sung Kim on 8/29/16.
//  Copyright © 2016 Sung Min Kim. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
